# LaPS Mail 16k
__WARNING:__  As transcrições desse corpora __NÃO__ são originais e foram 
criadas com auxílio do pocketsphinx. Portanto, há muitos erros de transcrição.
Dicionário e gramática no formado JSGF (que o CMU Sphinx aceita) foram criados e
encontram-se dentro da pasta `scripts`. Há também códigos para ajudar a ouvir o
áudio e consertar as transcrições. O modelo acústico utilizado encontra-se na
seção de recursos prontos para o CMU Sphinx no repositório GitLab do FalaBrasil
(CB).

## Descrição
Corpus de voz que representa o contexto de uma aplicação de correio eletrônico,
utilizado para a avaliação de sistemas LVCSR para tarefas de comando e controle.
Atualmente, composto por 86 sentenças (43 comando e 43 nomes próprios) gravados
por 25 voluntários (21 homens e 4 mulheres), o que corresponde a 84 minutos
minutos de áudio com um vocabulário de 95 palavras. As gravações foram
realizadas com um microfone de alta qualidade (Shure PG30) em um ambiente de
gravação não controlado.


## Descrição 2
Seção 5.1 da Dissertação de Mestrado de Rafael Oliveira (PPGCC, 2012):

>Neste trabalho foi iniciada a construção de um corpus de voz chamado LapsMail.
Esse corpus foi projetado para representar um conjunto básico de comandos
necessários para controlar uma aplicação de correio eletrônico. A ideia é
estabelecê-lo como referência para a avaliação de sistemas ASR para PB dentro
desse contexto. Atualmente, o corpus LapsMail consiste de 86 sentenças,
incluindo 43 comandos e 43 nomes falados por 25 voluntários (21 homens e 4
mulheres), o que corresponde a 84 minutos de áudio. Ao todo são observadas 95
palavras distintas. Segue abaixo três sentenças que fazem parte do corpus. 

```
(1) <s> abrir caixa de entrada </s>
(2) <s> responder ao remetente </s>
(3) <s> ana carolina </s>
```

O corpus LapsMail foi gravado usando um microfone de alta qualidade (Shure
PG30), amostrado em 16.000 Hz e quantizado em 16 bits. O ambiente acústico não
foi controlado, existindo a presença de ruído ambiente nas gravações.

## Lista de Sentenças
Comandos simples:     
```
fechar     sair       descartar      procurar      
spam       ler        pára           gravar              
```

Comandos compostos:     
```
abrir caixa de entrada               abrir email                       adicionar contato              
anexar arquivo                       anterior                          criar mensagem                       
criar nova mensagem                  deletar                           encaminhar                         
encaminhar mensagem                  enviar                            enviar para lixeira            
excluir mensagem                     exibir mensagens não lidas        inserir contato                
inserir destinatario                 ler mensagens                     mensagem anterior              
mover mensagem                       não lidas                         organizar por data             
organizar por ordem alfabética       organizar por remetente           primeira mensagem              
primeira não lida                    próxima                           próxima mensagem                
próxima não lida                     responder                         responder a todos                
responder ao remetente               salvar anexo                      salvar mensagem                
salvar rascunho                      última mensagem não lida          última mensagem recebida       
```

Nomes próprios:   
```
andrey          anderson      agnaldo          aldebaro       bruno         adalbery 
ana carolina    fernanda      nelson           josué          renan         danilo  
jonathas        lailson       gustavo          mariana        muller        diego  
diogo           sílvia        nagib            marcos         kelly         mônica
guilherme       william       jefferson        claudomir      lucila        pedro 
rodrigo         leonardo      claudio          fabiola        pelaes        rafael 
suane           charles       marcel           ericson        igor          cleyton       hugo
```
