#!/bin/bash
#
# Author: Feb 2019
# Cassio Batista - cassio.batista.13@gmail.com

ans_1="fair transcription performed by pocketsphinx"
ans_2="poor transcription quality produced by pocketsphinx"

for d in $(ls -d ../Mail*/)
do
	echo $d
	bash check_wav_transcription.sh $d
	echo "good quality? [Y/n]"
	read ans
	if [[ "$ans" == "n" ]] 
	then
		echo "$ans_2 -- CB ($(date))" > ${d}/README.md
	else
		echo "$ans_1 -- CB ($(date))" > ${d}/README.md
	fi
done
