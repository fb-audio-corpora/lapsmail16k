#!/bin/bash
#
# Author: Feb 2019
# Cassio Batista - cassio.batista.13@gmail.com

start=$(date)
for d in $(ls -d */) ; do
	echo "$d"
	for wav in $(ls ${d}/*.wav) ; do
		filebase=$(echo ${wav} | sed 's/.wav//g')
		tmpsent=$(tempfile)
		echo -e "\t${filebase}.wav"
		(pocketsphinx_continuous \
			-hmm  /home/cassio/fb-gitlab/fb-asr/fb-asr-resources/cmusphinx-resources/sen4000_gau16.cd_cont_4000 \
			-dict lapsmail_gram.dict \
			-fsg  lapsmail_gram.fsg \
			-samprate 16000 \
			-infile ${filebase}.wav 2> /dev/null > $tmpsent && \
			tail -n 1 $tmpsent > ${filebase}.txt && \
			rm $tmpsent) &
		sleep 0.2
	done
done
end=$(date)

echo $start
echo $end
