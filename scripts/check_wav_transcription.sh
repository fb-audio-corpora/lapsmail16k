#!/bin/bash
#
# Author: Feb 2019
# Cassio Batista - cassio.batista.13@gmail.com

if test $# -ne 1
then
	echo "Usage: arg 1 must be a dir"
	exit 1
elif [[ ! -d $1 ]]
then
	echo "Usage: arg 1 must be a dir"
	exit 1
fi

NUM_SAMPLES=10 # how many audios you wanna listen and check transcription?
for wav in $(ls ${1}/*.wav | sort -R | head -n $NUM_SAMPLES)
do
	filebase=$(echo $wav | sed 's/.wav//g')
	echo -e "\t $(cat ${filebase}.txt)"
	play $wav -q
done
